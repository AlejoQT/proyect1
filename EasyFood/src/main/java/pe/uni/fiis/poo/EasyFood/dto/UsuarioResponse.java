package pe.uni.fiis.poo.EasyFood.dto;

public class UsuarioResponse {
    private String usuario;
    private String msgError;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }
}
