package pe.uni.fiis.poo.EasyFood.service;

import pe.uni.fiis.poo.EasyFood.dto.UsuarioRequest;
import pe.uni.fiis.poo.EasyFood.dto.UsuarioResponse;

public interface MyService {
    UsuarioResponse loginUsuario(UsuarioRequest request);

}
