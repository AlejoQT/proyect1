package pe.uni.fiis.poo.EasyFood.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pe.uni.fiis.poo.EasyFood.dto.UsuarioRequest;
import pe.uni.fiis.poo.EasyFood.dto.UsuarioResponse;
import pe.uni.fiis.poo.EasyFood.service.MyService;

import java.awt.*;

@RestController
public class MyController {

    @Autowired
    private MyService myService;

    @PostMapping(value= "/usuario",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse loginUsuario(@RequestBody UsuarioRequest request){
        UsuarioResponse response = this.myService.loginUsuario(request);
        if(response.getUsuario() == null){
            response.setMsgError("No existe usuario");
        }
        return response;
    }


}
