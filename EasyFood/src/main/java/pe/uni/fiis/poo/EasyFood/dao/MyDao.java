package pe.uni.fiis.poo.EasyFood.dao;

import pe.uni.fiis.poo.EasyFood.domain.Usuario;

public interface MyDao {
    Usuario loginUsuario(Usuario request);

}
