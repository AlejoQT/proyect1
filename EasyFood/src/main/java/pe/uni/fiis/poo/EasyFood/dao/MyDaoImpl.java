package pe.uni.fiis.poo.EasyFood.dao;


import org.springframework.stereotype.Repository;
import pe.uni.fiis.poo.EasyFood.dao.datasource.MyDatasource;
import pe.uni.fiis.poo.EasyFood.domain.Usuario;
import pe.uni.fiis.poo.EasyFood.dto.UsuarioRequest;

@Repository
public class MyDaoImpl extends MyDatasource implements MyDao {


    public Usuario loginUsuario(Usuario request) {
        String sql =    " insert into Usuario (" +
                "       id_usuario,\n" +
                "       tipo_usuario,\n" +
                "       nombre,\n" +
                "       apellido,\n" +
                "       correo,\n"+
                "       clave)" +
                "       values( ? , ? , ? ,? , ?, ?)";
        this.jdbcTemplate.update(sql,
                new String[]{
                        request.getID_Usuario(),request.getTipo_Usuario(),request.getNombre(),
                        request.getApellido(),request.getCorreo(),request.getClave(),
                        "ACTIVE"
                });
        return request;
    }
}
